note
	description : "bd_test application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	FACTORY_SHARED

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l_database_appl: DATABASE_APPL [MYSQL]
			l_session_control: DB_CONTROL
		do
			create l_database_appl.login ("depart", "sou64lier")
			l_database_appl.set_application ("cati_virtuel")
			l_database_appl.set_base

			create l_session_control.make
			l_session_control.connect
			if l_session_control.is_connected then
				user_factory.fetch_by_id (1)
				if attached user_factory.item as la_item then
					print(la_item.out_32)
				else
					print("Not Found :(%N")
				end

				l_session_control.disconnect
			end


		end

end
