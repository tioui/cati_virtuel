note
	description: "{REPOSITORY} to fetch and store {VIDEO} object."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	VIDEO_REPOSITORY

inherit
	REPOSITORY
		redefine
			make
		end

create {REPOSITORIES_SHARED}
	make

feature {NONE} -- Initialization

	make(l_database_access:DB_ACCESS)
			-- <Precursor>
		do
			Precursor {REPOSITORY}(l_database_access)
			create filler.make (selection, prototype.twin)
			selection.set_action (filler)
		end

feature -- Access

	fetch_by_user_id(a_user_id:INTEGER)
			-- Get every {VIDEO} contructed by the {USER} having the `id' equal to `a_user_id'.
			-- The fetched objects can be retreive with `items'
		do
			execute_fetch("select * from `" + table_name + "` where `user_id` = '" +
										a_user_id.out + "'")
		ensure
			Fetched_Object_Valid: across items as la_items all la_items.item.user_id = a_user_id end
		end

feature {CONTROLLER} -- Implementation

	prototype:VIDEO
			-- <Precursor>
		once
			create Result
		end

feature {NONE} -- Implementation

	table_name:STRING
			-- <Precursor>
		once
			Result := "video"
		end

end
