note
	description: "[
						Get every {REPOSITORY} singleton.
						Inherit from this class to use a {REPOSITORY}
					]"
	author: "Louis Marchand"
	date: "$Date$"
	revision: "$Revision$"

class
	REPOSITORIES_SHARED

feature {NONE} -- Access

	link_repository:LINK_REPOSITORY
			-- {REPOSITORY} to manage {LINK} object
		once
			create Result.make (database_access)
		end

	user_repository:USER_REPOSITORY
			-- {REPOSITORY} to manage {USER} object
		once
			create Result.make (database_access)
		end

	video_repository:VIDEO_REPOSITORY
			-- {REPOSITORY} to manage {VIDEO} object
		once
			create Result.make (database_access)
		end

	video_file_repository:VIDEO_FILE_REPOSITORY
			-- {REPOSITORY} to manage {VIDEO_FIE} object
		once
			create Result.make (database_access)
		end

	database_access:DB_ACCESS
			-- The database session manager
		once
			create Result
		end

end
