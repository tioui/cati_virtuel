note
	description: "{REPOSITORY} to fetch and store {VIDEO_FILE} object."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	VIDEO_FILE_REPOSITORY

inherit
	REPOSITORY
		redefine
			make
		end

create {REPOSITORIES_SHARED}
	make

feature {NONE} -- Initialization

	make(l_database_access:DB_ACCESS)
			-- <Precursor>
		do
			Precursor {REPOSITORY}(l_database_access)
			create filler.make (selection, prototype.twin)
			selection.set_action (filler)
		end

feature -- Access

	fetch_with_poster_by_video_id(a_video_id:INTEGER)
			-- Get every {VIDEO_FILE} object from the database associate to the {VIDEO} having the `id'
			-- equal to `a_video_id'. The fetched objects can be retreive with `items'
		do
			execute_fetch("select * from `" + table_name + "` where `video_id` = '" +
										a_video_id.out + "'")
		ensure
			Fetched_Object_Valid: across items as la_items all la_items.item.video_id = a_video_id end
		end

	fetch_by_video_id(a_video_id:INTEGER)
		do
			execute_fetch("select * from `" + table_name + "` where `video_id` = '" +
										a_video_id.out + "' and `format` != 'poster'")
		ensure
			Fetched_Object_Valid: across items as la_items all
										la_items.item.video_id = a_video_id and
										not la_items.item.format.same_string ("poster")
									end
		end

	fetch_poster_by_video_id(a_video_id:INTEGER)
		do
			execute_fetch("select * from `" + table_name + "` where `video_id` = '" +
										a_video_id.out + "' and `format` = 'poster'")
		ensure
			Fetched_Object_Valid: 	across items as la_items all
										la_items.item.video_id = a_video_id and
										la_items.item.format.same_string ("poster")
									end
		end

feature {CONTROLLER} -- Implementation

	prototype:VIDEO_FILE
			-- <Precursor>
		once
			create Result
		end


feature {NONE} -- Implementation

	table_name:STRING
			-- <Precursor>
		once
			Result := "video_file"
		end

end
