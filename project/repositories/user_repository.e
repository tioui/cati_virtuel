note
	description: "{REPOSITORY} to fetch and store {USER} object."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	USER_REPOSITORY

inherit
	REPOSITORY
		redefine
			make
		end

create {REPOSITORIES_SHARED}
	make

feature {NONE} -- Initialization

	make(l_database_access:DB_ACCESS)
			-- <Precursor>
		do
			Precursor {REPOSITORY}(l_database_access)
			create filler.make (selection, prototype.twin)
			selection.set_action (filler)
		end


feature -- Access

	fetch_by_username_and_password(a_username, a_password:READABLE_STRING_GENERAL)
			-- Get the {USER} that has {USER}.`username' equal to `a_username' and {USER}.`password'
			-- equal to `a_password'. The fetched object can be retreive with `item'
		do
			execute_fetch("select * from `" + table_name + "` where `username` = '" +
										a_username + "' and `password` = '" + a_password + "'")
		ensure
			Fetched_Object_Valid: attached item as la_item implies
										(la_item.username.same_string (a_username) and
										la_item.password.same_string (a_password))
		end

	fetch_by_username(a_username:READABLE_STRING_GENERAL)
			-- Get the {USER} that has {USER}.`username' equal to `a_username'. The fetched
			-- object can be retreive with `item'
		do
			execute_fetch("select * from `" + table_name + "` where `username` = '" +
										a_username + "'")
		ensure
			Fetched_Object_Valid: attached item as la_item implies la_item.username.same_string (a_username)
		end

feature {CONTROLLER} -- Implementation

	prototype:USER
			-- <Precursor>
		once
			create Result
		end

feature {NONE} -- Implementation

	table_name:STRING
			-- <Precursor>
		once
			Result := "user"
		end

end
