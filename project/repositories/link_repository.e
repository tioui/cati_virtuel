note
	description: "{REPOSITORY} to fetch and store {LINK} object."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	LINK_REPOSITORY

inherit
	REPOSITORY
		redefine
			make
		end

create {REPOSITORIES_SHARED}
	make

feature {NONE} -- Initialization

	make(a_database_access:DB_ACCESS)
			-- <Precursor>
		do
			Precursor {REPOSITORY}(a_database_access)
			create filler.make (selection, prototype.twin)
			selection.set_action (filler)
		end

feature -- Access

	fetch_by_video_id(a_video_id:INTEGER)
			-- Get every {LINK} object from the database associate to the {VIDEO} having the `id'
			-- equal to `a_video_id'. The fetched objects can be retreive with `items'
		do
			execute_fetch("select * from `" + table_name + "` where `video_id` = '" +
										a_video_id.out + "'")
		ensure
			Fetched_Object_Valid: across items as la_items all la_items.item.video_id = a_video_id end
		end

feature {CONTROLLER} -- Implementation

	prototype:LINK
			-- <Precursor>
		once
			create Result
		end

feature {NONE} -- Implementation

	table_name:STRING
			-- <Precursor>
		once
			Result := "link"
		end
end
