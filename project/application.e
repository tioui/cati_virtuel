note
	description : "The system root class"
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

deferred class
	APPLICATION

inherit
	DATABASE_APPL [MYSQL]
		rename
			login as database_login,
			set_application as set_schema
		end
	WSF_DEFAULT_SERVICE[EXECUTION]
		redefine
			initialize
		end

feature {NONE} -- Initialization

	initialize
			-- Initialization of the system (setting web parameters and database informations)
		do
			create {WSF_SERVICE_LAUNCHER_OPTIONS_FROM_INI} service_options.make_from_file (serveur_ini_name)
			database_login ("depart", "wla5OAthIAtiexl3wouBroexiew2Ef")
			set_schema ("cati_virtuel")

			set_base
			Precursor
		end

feature {NONE} -- Implementation

	serveur_ini_name:READABLE_STRING_GENERAL
			-- The file name of the ini file contining server options
		deferred
		end

end
