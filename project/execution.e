note
	description: "Summary description for {EXECUTION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EXECUTION

inherit
	WSF_ROUTED_EXECUTION
	WSF_ROUTED_URI_TEMPLATE_HELPER

create
	make

feature {NONE} -- Initialization

	setup_router
			-- Map every route to the corresponding {CONTROLLER}
		do
			map_uri_template_response_agent ("", agent index_response, Void)
			map_uri_template_response_agent ("/", agent index_response, Void)
			map_uri_template ("/video/", create {VIDEO_CONTROLLER}, router.methods_GET_POST)
			map_uri_template ("/video/{type}/", create {VIDEO_CONTROLLER}, router.methods_GET_POST)
			map_uri_template ("/video/{type}/{video_id}", create {VIDEO_CONTROLLER}, router.methods_GET_POST)
			map_uri_template ("/user/", create {USER_CONTROLLER}, router.methods_GET)
			map_uri_template ("/user/{type}", create {USER_CONTROLLER}, router.methods_GET_POST)
			map_uri_template ("/user/{type}/{user_id}", create {USER_CONTROLLER}, router.methods_GET_POST)
		end

feature {NONE} -- Implementation

	index_response (a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
		do
			create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/video/list/"))
		end


end
