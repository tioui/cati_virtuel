note
	description: "The system root class when using CGI server connector (for Apache)"
	author: "Louis Marchand"
	date: "The system root class"
	revision: "1.0"

class
	APPLICATION_CGI

inherit
	APPLICATION

create
	make_and_launch

feature {NONE} -- Implementation

	serveur_ini_name:READABLE_STRING_GENERAL
			-- The file name of the ini file contining server options
		do
			Result := "cgi.ini"
		end

end
