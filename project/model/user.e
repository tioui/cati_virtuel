note
	description: "An administrator of the system."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	USER

inherit
	MODEL
		rename
			repository as user_repository
		redefine
			default_create,
			out_32
		end

create
	default_create

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {MODEL}
			set_username ("")
			set_password ("")
			set_first_name ("")
			set_last_name ("")
			set_email ("")
			set_skype_account ("")
			unset_has_hangout_account
		end

feature -- Access

	username: READABLE_STRING_GENERAL
			-- The unique identifiant of `Current'

	password: READABLE_STRING_GENERAL
			-- Private text to enter when `Current' log in.

	first_name: READABLE_STRING_GENERAL
			-- The first part of real name of `Current'

	last_name: READABLE_STRING_GENERAL
			-- The last part of real name of `Current'

	email: READABLE_STRING_GENERAL
			-- The electronic mail address of `Current'

	skype_account:READABLE_STRING_GENERAL
			-- The Skype account name of `Current'

	has_hangout_account:BOOLEAN
			-- True if `Current'.`email' is associate to a Google Hangout account
		do
			Result := has_hangout /= 0
		end

	videos:LIST[VIDEO]
			-- Every {VIDEO} created by `Current'
		do
			video_repository.fetch_by_user_id (id)
			if attached video_repository.items as la_items then
				Result := la_items
			else
				create {ARRAYED_LIST[VIDEO]}Result.make (0)
			end
		end

feature -- Settings

	set_username (a_username: READABLE_STRING_GENERAL)
			-- Assign the value of `username' to `a_username'
		require
			value_exists: a_username /= Void
		do
			username := a_username
		ensure
			username_set: a_username.same_string (username)
		end

	set_password (a_password: READABLE_STRING_GENERAL)
			-- Assign the value of `password' to `a_password'
		require
			value_exists: a_password /= Void
		do
			password := a_password
		ensure
			password_set: a_password.same_string (password)
		end

	set_first_name (a_first_name: READABLE_STRING_GENERAL)
			-- Assign the value of `first_name' to `a_first_name'
		require
			value_exists: a_first_name /= Void
		do
			first_name := a_first_name
		ensure
			first_name_set: a_first_name.same_string (first_name)
		end

	set_last_name (a_last_name: READABLE_STRING_GENERAL)
			-- Assign the value of `last_name' to `a_last_name'
		require
			value_exists: a_last_name /= Void
		do
			last_name := a_last_name
		ensure
			last_name_set: a_last_name.same_string (last_name)
		end

	set_email (a_email: READABLE_STRING_GENERAL)
			-- Assign the value of `email' to `a_email'
		require
			value_exists: a_email /= Void
		do
			email := a_email
		ensure
			email_set: a_email.same_string (email)
		end

	set_skype_account (a_skype_account: READABLE_STRING_GENERAL)
			-- Assign the value of `skype_account' to `a_skype_account'
		require
			value_exists: a_skype_account /= Void
		do
			skype_account := a_skype_account
		ensure
			skype_account_set: a_skype_account.same_string (skype_account)
		end

	set_has_hangout_account
			-- Set the value of `has_hangout' to `True'
		do
			set_has_hangout(1)
		ensure
			has_hangout_set: has_hangout_account = True
		end

	unset_has_hangout_account
			-- Set the value of `has_hangout' to `False'
		do
			set_has_hangout(0)
		ensure
			has_hangout_set: has_hangout_account = False
		end

feature -- Output

	out_32: STRING_32
			-- <Precursor>
		do
			Result := Precursor {MODEL}
			Result.append (username.out + "%N")
			Result.append (password.out + "%N")
			Result.append (first_name.out + "%N")
			Result.append (last_name.out + "%N")
			Result.append (email.out + "%N")
			Result.append (has_hangout.out + "%N")
		end

feature {USER} -- Implementation

	has_hangout: INTEGER
			-- Has a value of `0' if `Current'.`email' is not assign to a Google Account

	set_has_hangout(a_has_hangout: INTEGER)
			-- Assign the value of `has_hangout' to `a_has_hangout'
		do
			has_hangout := a_has_hangout
		ensure
			has_hangout_set: has_hangout = a_has_hangout
		end

end -- class USER
