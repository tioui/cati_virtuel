note
	description: "Class which allows EiffelStore to retrieve/store%
	      %the content relative to a column of the table VIDEO_FILE"

class
	VIDEO_FILE

inherit
	MODEL
		rename
			repository as video_file_repository
		redefine
			default_create,
			out_32
		end

create
	default_create


feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {MODEL}
			address := ""
			format := ""
			file_order := 0
			video_id := 0
		end

feature -- Access

	address: READABLE_STRING_GENERAL
			-- One of the URLs to the {VIDEO}

	format: READABLE_STRING_GENERAL
			-- The type (codec and format) of the `Current'

	file_order: INTEGER
			-- In what position must be `Current' in the Web video viewer.

	video:VIDEO
			-- The {VIDEO} that `Current' is include in.
		do
			video_repository.fetch_by_id (video_id)
			if attached video_repository.item as la_video then
				Result := la_video
			else
				create Result
			end
		ensure
			Video_Is_Valid: Result.id = video_id
		end
feature -- Settings

	set_address (a_address: READABLE_STRING_GENERAL)
			-- Assign the value of `address' to `a_address'
		require
			value_exists: a_address /= Void
		do
			address := a_address
		ensure
			address_set: a_address.same_string (address)
		end

	set_format (a_format: READABLE_STRING_GENERAL)
			-- Assign the value of `format' to `a_format'
		require
			value_exists: a_format /= Void
		do
			format := a_format
		ensure
			type_set: a_format.same_string (format)
		end

	set_file_order (a_order: INTEGER)
			-- Assign the value of `order' to `a_order'
		do
			file_order := a_order
		ensure
			order_set: a_order = file_order
		end

	set_video(a_video:VIDEO)
			-- Assign the value of `video' to `a_video'
		do
			set_video_id (a_video.id)
		ensure
			Video_Is_Set: a_video ~ video
		end


feature {DATABASE_SESSION_MANAGER_ACCESS, MODEL, REPOSITORY}

	video_id: INTEGER
			--What is the `id' of `video'

	set_video_id (a_video_id: INTEGER)
			-- Assign the value of `video_id' to `a_video_id'
		do
			video_id := a_video_id
		ensure
			video_id_set: a_video_id = video_id
		end

feature -- Output

	out_32: STRING_32
			-- <Precursor>
		do
			Result := Precursor {MODEL}
			Result.append (address.out + "%N")
			Result.append (format.out + "%N")
			Result.append (file_order.out + "%N")
			Result.append (video_id.out + "%N")
		end

end -- class VIDEO_FILE
