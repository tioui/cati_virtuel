note
	description: "Adaptation of the {LINK} to be use in the create_video and edit_video views."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	LINK_VIDEO_EDIT_VIEW_MODEL

inherit
	LINK
		redefine
			default_create
		end

create
	make,
	default_create

feature {NONE} -- Initialisation

	make(a_link:LINK; a_no:INTEGER)
			-- Initialization of `Current' based on `a_link' and using `a_no' as view number.
		local
			l_converter:UTF_CONVERTER
		do
			create l_converter
			id := a_link.id
--			title := l_converter.string_32_to_utf_8_string_8 (a_link.title.to_string_32)
			title := a_link.title
			address := a_link.address
			from_second := a_link.from_second
			to_second := a_link.to_second
			duration := to_second - from_second
			size := a_link.size
			no := a_no
		end

	default_create
			-- Initialization of `Current'
		do
			Precursor {LINK}
			duration := to_second - from_second
			no :=0
		end

feature -- Access

	no:INTEGER
			-- The number of `Current' used in the view

	set_no(a_no:INTEGER)
			-- Assing `a_no' to `no'
		do
			no := a_no
		ensure
			No_Set: no = a_no
		end

	duration:INTEGER
			-- the number on seconds between the showing of `Current' (`from_second') and the hiding (`to_second')

	model_representation:LINK
			-- The {VIDEO_FILE} object represented by `Current'
		do
			create Result
			Result.set_address (address)
			Result.set_from_second (from_second)
			Result.set_id (id)
			Result.set_size (size)
			Result.set_title (title)
			Result.set_to_second (from_second + duration)
			Result.set_video_id (video_id)
		end


end
