note
	description: "Adaptation of the {USER} to be use in the create_user and edit_user views."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	USER_VIDEO_LIST_VIEW_MODEL

inherit
	USER
		rename
			has_hangout_account as user_has_hangout_account
		end

create
	make

feature {NONE} -- Initialization

	make(a_user:USER; a_selected_id:INTEGER)
			-- Initialization for `Current' based on `a_user' and is selected in the view
			-- if `a_selected_id' is equal to `a_user'.`id'.
		do
			id := a_user.id
			username := a_user.username
			password := a_user.password
			first_name := a_user.first_name
			last_name := a_user.last_name
			email := a_user.email
			has_hangout := a_user.has_hangout
			is_selected := id = a_selected_id
			skype_account := a_user.skype_account
			has_hangout_account := user_has_hangout_account
		end

feature -- Access

	is_selected:BOOLEAN
			-- `Current' is the selected one in the view.

	has_hangout_account:BOOLEAN
			-- `Current' has an hangout account

end
