note
	description: "Summary description for {VIDEO_VIDEO_EDIT_VIEW_MODEL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VIDEO_VIDEO_EDIT_VIEW_MODEL

inherit
	VIDEO
		redefine
			default_create
		end

create
	make,
	default_create

feature {NONE} -- Initialisation

	make(a_video:VIDEO)
			-- Initialization of `Current' based on `a_video'.
		local
			l_converter:UTF_CONVERTER
		do
			create l_converter
			id := a_video.id
--			title := l_converter.string_32_to_utf_8_string_8 (a_video.title.to_string_32)
			title := a_video.title
			width := a_video.width
			height := a_video.height
			user_id := a_video.user_id
		end

	default_create
			-- Initialization of `Current'
		do
			Precursor {VIDEO}
		end

end
