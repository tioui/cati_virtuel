note
	description: "Adaptation of the {VIDEO_FILE} to be use in the create_video and edit_video views."
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	FILE_VIDEO_EDIT_VIEW_MODEL

inherit
	VIDEO_FILE
		redefine
			default_create
		end

create
	make,
	default_create

feature {NONE} -- Initialization

	make(a_file:VIDEO_FILE; a_no:INTEGER)
			-- Initialization of `Current' based on `a_file' and using `a_no' as view number.
		do
			id := a_file.id
			address :=a_file.address
			format := a_file.format
			file_order := a_file.file_order
			no:=a_no
		end

	default_create
			-- Initialization of `Current'
		do
			Precursor {VIDEO_FILE}
			no :=0
		end

feature -- Access

	no:INTEGER
			-- The number of `Current' used in the view

	set_no(a_no:INTEGER)
			-- Assing `a_no' to `no'
		do
			no := a_no
		ensure
			No_Set: no = a_no
		end

	model_representation:VIDEO_FILE
			-- The {VIDEO_FILE} object represented by `Current'
		do
			create Result
			Result.set_id (id)
			Result.set_address (address)
			Result.set_file_order (file_order)
			Result.set_format (format)
			Result.set_video_id (video_id)
		end

end
