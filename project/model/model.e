note
	description: "Ancestor of every model class of the system (the field should be equivalent to the table on the DataBase)"
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

deferred class
	MODEL

inherit
	REPOSITORIES_SHARED
		redefine
			is_equal,
			default_create,
			out
		end

feature {NONE} -- Initialization

	default_create
			-- Initialisation of `Current'
		do
			set_id(0)
		end

feature -- Settings

	id: INTEGER
			-- Unique identifier of `Current'

	is_equal (other: like Current): BOOLEAN
			-- <Precursor>
		do
			Result := id = other.id
		end

	out_32: STRING_32
			-- Test representation of `Current'
		do
			Result := {STRING_32}""
			Result.append (id.out + "%N")
		end

	out:STRING
			-- <Precursor>
		do
			Result := out_32.to_string_8
		end

	save
			-- Insert or update `Current' in the database
		do
			if id = 0 then
				repository.store.put (Current)
				set_id (database_access.last_inserted_id)
			else
				repository.store.update (Current)
			end
		ensure
			Is_In_BD: id > 0
		end

	delete
			-- Remove `Current' from the Database
		do
			if id /= 0 then
				repository.store.delete (Current)
				set_id (0)
			end
		end

feature {DATABASE_SESSION_MANAGER_ACCESS, MODEL} -- Implementation

	set_id (a_id: INTEGER)
			-- Assign the unique identifier of `Current'
		do
			id := a_id
		ensure
			id_set: a_id = id
		end

feature {NONE} -- Implementation

	repository:REPOSITORY
			-- {REPOSITORY} managing object like `Current'
		deferred
		end

end
