note
	description: "An hyperlink embeded in a {VIDEO}"
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	LINK

inherit
	MODEL
		rename
			repository as link_repository
		redefine
			default_create,
			out_32
		end

create
	default_create

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {MODEL}
			set_title ("")
			set_address ("")
			from_second := 0
			to_second := 0
			size := 0
			video_id := 0
		end

feature -- Access

	title: READABLE_STRING_GENERAL
			-- The replacement text when showing `Current'

	address: READABLE_STRING_GENERAL
			-- The URL that `Current' is pointing

	from_second: INTEGER
			-- From which second `Current' has to be show

	to_second: INTEGER
			-- From which second `Current' has to be hide

	size: INTEGER
			-- The font size of the showed `title'

	video:VIDEO
			-- The {VIDEO} that `Current' must be showed in.
		do
			video_repository.fetch_by_id (video_id)
			if attached video_repository.item as la_video then
				Result := la_video
			else
				create Result
			end
		ensure
			Video_Is_Valid: Result.id = video_id
		end

feature -- Settings
	set_title (a_title: READABLE_STRING_GENERAL)
			-- Assign the value of `title' to `a_title'
		require
			value_exists: a_title /= Void
		do
			title := a_title
		ensure
			title_set: a_title.same_string (title)
		end

	set_address (a_address: READABLE_STRING_GENERAL)
			-- Assign the value of `address' to `a_address'
		require
			value_exists: a_address /= Void
		do
			address := a_address
		ensure
			address_set: a_address.same_string (address)
		end

	set_from_second (a_from_second: INTEGER)
			-- Assign the value of `from_second' to `a_from_second'
		do
			from_second := a_from_second
		ensure
			from_second_set: a_from_second = from_second
		end

	set_to_second (a_to_second: INTEGER)
			-- Assign the value of `to_second' to `a_to_second'
		do
			to_second := a_to_second
		ensure
			to_second_set: a_to_second = to_second
		end

	set_size (a_size: INTEGER)
			-- Assign the value of `size' to `a_size'
		do
			size := a_size
		ensure
			size_set: a_size = size
		end

	set_video(a_video:VIDEO)
			-- Assign the value of `video' to `a_video'
		do
			set_video_id (a_video.id)
		ensure
			Video_Is_Set: a_video ~ video
		end

feature {DATABASE_SESSION_MANAGER_ACCESS, MODEL, REPOSITORY}

	video_id: INTEGER
			-- The `id' of `video'

	set_video_id (a_video_id: INTEGER)
			-- Assign the value of `video_id' to `a_video_id'
		do
			video_id := a_video_id
		ensure
			video_id_set: a_video_id = video_id
		end

feature -- Output

	out_32: STRING_32
			-- <Precursor>
		do
			Result := Precursor {MODEL}
			Result.append (title.out + "%N")
			Result.append (address.out + "%N")
			Result.append (from_second.out + "%N")
			Result.append (to_second.out + "%N")
			Result.append (size.out + "%N")
			Result.append (video_id.out + "%N")
		end


end -- class LINK
