note
	description: "Class which allows EiffelStore to retrieve/store%
	      %the content relative to a column of the table VIDEO"

class
	VIDEO

inherit
	MODEL
		rename
			repository as video_repository
		redefine
			default_create,
			out_32
		end

create
	default_create

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {MODEL}
			title := ""
			width := 0
			height := 0
			user_id := 0
		end

feature -- Access

	title: READABLE_STRING_GENERAL
			-- A text description of `Current'

	width: INTEGER
			-- The default horizontal lenght of the viewer showing `Current'

	height: INTEGER
			-- The default vertical lenght of the viewer showing `Current'

	user_id: INTEGER
			-- The `id' of the `creator'

	creator:USER
			-- The {USER} that has create `Current'
		do
			user_repository.fetch_by_id (user_id)
			if attached user_repository.item as la_user then
				Result := la_user
			else
				create Result
			end
		ensure
			Creator_Is_Valid: Result.id = user_id
		end

	files:LIST[VIDEO_FILE]
			-- Every {VIDEO_FILE} associate to `Current' excluding the `poster'
		do
			video_file_repository.fetch_by_video_id (id)
			if attached video_file_repository.items as la_items then
				Result := la_items
			else
				create {ARRAYED_LIST[VIDEO_FILE]}Result.make (0)
			end
		end

	files_and_poster:LIST[VIDEO_FILE]
			-- Every {VIDEO_FILE} associate to `Current' including the poster
		do
			video_file_repository.fetch_with_poster_by_video_id (id)
			if attached video_file_repository.items as la_items then
				Result := la_items
			else
				create {ARRAYED_LIST[VIDEO_FILE]}Result.make (0)
			end
		end

	links:LIST[LINK]
			-- Every {LINK} associate to `Current'
		do
			link_repository.fetch_by_video_id (id)
			if attached link_repository.items as la_items then
				Result := la_items
			else
				create {ARRAYED_LIST[LINK]}Result.make (0)
			end
		end

	poster:detachable READABLE_STRING_GENERAL
			-- The URL of an image file to put on `Current' before playing
		do
			video_file_repository.fetch_poster_by_video_id (id)
			if attached video_file_repository.item as la_item then
				Result := la_item.address
			end
		end

feature -- Settings

	set_title (a_title: READABLE_STRING_GENERAL)
			-- Assign the value of `title' to `a_title'
		require
			value_exists: a_title /= Void
		do
			title := a_title
		ensure
			title_set: a_title.same_string (title)
		end

	set_width (a_width: INTEGER)
			-- Assign the value of `width' to `a_width'
		do
			width := a_width
		ensure
			width_set: a_width = width
		end

	set_height (a_height: INTEGER)
			-- Assign the value of `height' to `a_height'
		do
			height := a_height
		ensure
			height_set: a_height = height
		end

	set_user_id (a_user_id: INTEGER)
			-- Assign the value of `user_id' to `a_user_id'
		do
			user_id := a_user_id
		ensure
			user_id_set: a_user_id = user_id
		end

	set_creator(a_creator:USER)
			-- Assign the value of `creator' to `a_creator'
		do
			set_user_id (a_creator.id)
		ensure
			Creator_Is_Set: a_creator ~ creator
		end


feature -- Output

	out_32: STRING_32
			-- <Precursor>
		do
			Result := Precursor {MODEL}
			Result.append (title.out + "%N")
			Result.append (width.out + "%N")
			Result.append (height.out + "%N")
			Result.append (user_id.out + "%N")
		end

end -- class VIDEO
