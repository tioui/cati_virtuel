{include file="view/header.tpl"}{assign name="title" value="Confirmation" /}{/include}
{include file="view/menu.tpl"}{/include}
	<div class="container">
		<div class="content_with_menu">
			{if condition="$error"}
				<div class="alert-danger" role="alert">
					Le video n'a pas pu �tre d�truit.
				</div>
			{/if}
			{unless condition="$error"}
				<h1>Le vid�o "{$video.title/}" a �t� cr�� supprim� avec succes.</h1>
			{/unless}

			<p><a href="{$script_url/}/video/list/">Retourner � la liste des vid�os</a></p>
		</div>
	</div>
{include file="view/footer.tpl"/}
