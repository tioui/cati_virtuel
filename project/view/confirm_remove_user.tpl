{include file="view/header.tpl"}{assign name="title" value="Confirmation" /}{/include}
{include file="view/menu.tpl"}{/include}
	<div class="container">
		<div class="content_with_menu">
			{if condition="$same_user"}
				<div id="error-self_destruct" class="alert-danger" role="alert">
					Vous ne pouvez pas vous d�truire vous-m�me.
				</div>
			{/if}

			{if condition="$error"}
				<div class="alert-danger" role="alert">
					L'administrateur n'a pas pu �tre d�truit.
				</div>
			{/if}
			{unless condition="$error"}
				<div class="hidden" name="success"></div>
				<h1>L'administrateur {$user.first_name/} {$user.last_name/} a �t� cr�� supprim� succes.</h1>
			{/unless}

			<p><a href="{$script_url/}/user/list/">Retourner � la liste des administrateurs</a></p>
		</div>
	</div>
{include file="view/footer.tpl"/}
