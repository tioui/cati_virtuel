{include file="view/header.tpl"}{assign name="title" value="Liste des utilisateurs" /}{/include}
{include file="view/menu.tpl"}{assign name="is_list_user" value="True"/}{/include}
	<div class="container">
		<div class="content_with_menu">
			<H1>Liste des utilisateurs</H1>
			<a href="{$script_url/}/user/create/">Cr�er un nouvel administrateur</a>
			<table class="table">
				<tr>
					<th>
						Nom d'usager
					</th>
					<th>
						Nom	
					</th>
					<th>
						Courriel
					</th>
					<th>
						Compte Google Hangout
					</th>
					<th>
						Nom Skype 
					</th>
					<th>
					</th>
				</tr>
				{foreach item="user" from="$user_list"}
				<tr>
					<td class="username_list_field">
						<div class="hidden id_list_field" id="id_list_field_{$user.id/}">{$user.id/}</div>
						{$user.username/}
					</td>
					<td>
						<div class="hidden first_name_list_field" id="first_name_list_field_{$user.id/}">{$user.first_name/}</div>
						<div class="hidden last_name_list_field" id="last_name_list_field_{$user.id/}">{$user.last_name/}</div>
						{$user.first_name/} {$user.last_name/}
					</td>
					<td class="email_list_field">
						{$user.email/}
					</td>
					<td>
						<div class="hidden has_hangout_list_field" id="has_hangout_list_field_{$user.id/}">
							{if condition="$user.has_hangout_account"}
								yes
							{/if}
							{unless condition="$user.has_hangout_account"}
								no
							{/unless}
						</div>
						{if condition="$user.has_hangout_account"}
							Oui
						{/if}
						{unless condition="$user.has_hangout_account"}
							Non
						{/unless}
					</td>
					<td class="skype_account_list_field">
						{$user.skype_account/}
					</td>
					<td>
						<a name="edit_link{$user.id/}" href="{$script_url/}/user/edit/{$user.id/}/">Modifier</a> | <a name="delete_link{$user.id/}" href="{$script_url/}/user/remove/{$user.id/}/">Supprimer</a> 
					</td>
				</tr>
				{/foreach}
			</table>
		</div>
	</div>
{include file="view/footer.tpl"/}
