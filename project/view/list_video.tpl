{include file="view/header.tpl"}{assign name="title" value="Liste des vid�os" /}{/include}
{include file="view/menu.tpl"}{assign name="is_list_video" value="True"/}{/include}
	<div class="container">
		<div class="content_with_menu">
			<H1>Liste des vid�os</H1>
			<table class="form-table">
				<tr>
					<td>
						<form method="post" action="{$script_url/}/video/list/">
							<select name="user_id">
								{foreach item="user" from="$user_list"}
									<option value="{$user.id/}" {if condition="$user.is_selected"}selected{/if}> {$user.first_name/} {$user.last_name/} </option>
								{/foreach}
							</select>
							<input type="submit" value="Visualiser"/>
						</form>
					</td>
				</tr>
			</table>
			<br/><br/><br/>
			<a href="{$script_url/}/video/create/">Cr�er un nouveau vid�o</a>
			<table class="table">
				<tr>
					<th>Titre</th>
					<th></th>
				</tr>
				{foreach item="video" from="$video_list"}
				<tr>
					<td>
						{$video.title/}
					</td>
					<td>
						
						<a href="{$script_url/}/video/view/{$video.id/}/">Visionner</a> | <a href="{$script_url/}/video/edit/{$video.id/}/">Modifier</a> | <a href="{$script_url/}/video/address/{$video.id/}/">Adresse</a> | <a href="{$script_url/}/video/remove/{$video.id/}/">Supprimer</a>
					</td>
				</tr>
				{/foreach}
			</table>
		</div>
	</div>
{include file="view/footer.tpl"/}
