{include file="view/header.tpl"}{assign name="title" value="Modification d'un vid�o" /}{/include}
{include file="view/menu.tpl"}{/include}
	<div class="container">
		<div class="content_with_menu">
			<H1>Modification d'un vid�o</H1>

			{if condition="$has_error"}
				<div class="alert-danger" role="alert">
					Une erreur est survenue.
				</div>
			{/if}
			<form method="post" action="{$script_url/}/video/edit/{$video.id/}/">
				<input type="hidden" name="id" value="{$video.id/}">
				<input type="hidden" name="user_id" value="{$video.user_id/}">
				<table class="form-table">
					<tr>
						<td>
							Titre:
						</td>
						<td>
							<input type="text" name="title" value="{$video.title/}">
						</td>
					</tr>
					<tr>
						<td>
							Largeur:
						</td>
						<td>
							<input type="text" name="width" value="{$video.width/}">
						</td>
					</tr>
					<tr>
						<td>
							Hauteur:
						</td>
						<td>
							<input type="text" name="height" value="{$video.height/}">
						</td>
					</tr>
				</table>
				<h2>Fichier vid�o</h2>
				<input type="hidden" name="nb_file" value="{$nb_file/}">
				<table class="form-table">
					{foreach from="$file_list" item="file"}
						<tr>
							<td>Adresse: <input type="text" name="file_address{$file.no/}" value="{$file.address/}"><input type="hidden" name="file_id{$file.no/}" value="{$file.id/}"></td>
							<td>Type: <input type="text" name="file_format{$file.no/}" value="{$file.format/}"></td>
							<td>Ordre: <input size="1" type="text" name="file_file_order{$file.no/}" value="{$file.file_order/}"></td>
							<td><input class="imgBtnDel" type="submit" name="file_remove{$file.no/}" value=""></td>
						</tr>
					{/foreach}
				</table>
				<input type="hidden" name="nb_link" value="{$nb_link/}">
				<input type="submit" class="btn btn-lg btn-default" name="add_file" value="Ajouter un fichier video">
				<H2>Lien hypervid�o</h2>
				<table class="form-table">
					{foreach from="$link_list" item="link"}
						<tr>
							<td>
								Titre: <input type="text" name="link_title{$link.no/}" value="{$link.title/}"><input type="hidden" name="link_id{$link.no/}" value="{$link.id/}">
							</td>
							<td>Adresse: <input type="text" name="link_address{$link.no/}" value="{$link.address/}"></td>
							<td>Size: <input size="2" type="text" name="link_size{$link.no/}" value="{$link.size/}"></td>
							<td>� la Seconde: <input size="3" type="text" name="link_from_second{$link.no/}" value="{$link.from_second/}"></td>
							<td>Dur�e: <input size="2" type="text" name="link_duration{$link.no/}" value="{$link.duration/}"></td>
							<td><input type="submit" class="imgBtnDel" name="link_remove{$link.no/}" value=""></td>
						</tr>
					{/foreach}
				</table>
				<input type="submit" class="btn btn-lg btn-default" name="add_link" value="Ajouter un lien"><br/>
<br/><br/>
				<center>
					<input type="submit" class="btn btn-lg btn-default" name="modify" value="Modifier">
					<input type="submit" class="btn btn-lg btn-default" name="reset" value="R�initialiser">
					<input type="submit" class="btn btn-lg btn-default" name="cancel" value="Cancel">
				</center>


			</form>
		</div>
	</div>
{include file="view/footer.tpl"/}
