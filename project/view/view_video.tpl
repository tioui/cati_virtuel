<!DOCTYPE html>
<html>
<head>
  <meta charset="ISO-8859-1">
  <title>{$video.title/}</title>

  <link href="http://cati.lmarchand.profweb.ca/video-link.css" rel="stylesheet">
  <link href="http://vjs.zencdn.net/4.1/video-js.css" rel="stylesheet">
  <script src="http://vjs.zencdn.net/4.1/video.js"></script>
  <script src="http://cati.lmarchand.profweb.ca/video-link.js"></script>

</head>
<body>
	{if condition="$user_connect"}
 	<table cellspacing=10>
		<tr>
			<td>
				<a href="{$script_url/}/video/list/">Voir la liste des vid�os</a>
			</td>
			<td>
				<a href="{$script_url/}/user/list/">Voir la liste des administrateurs</a>
			</td>
			<td>
				<a href="{$script_url/}/user/logout/">D�connexion</a>
			</td>
		</tr>
	</table>
	{/if}
	 <H1>{$video.title/}</H1>

  <video id="vid1" class="video-js vjs-default-skin" {if condition="$autoplay"}autoplay{/if} controls preload="auto" width="{$video.width/}" height="{$video.height/}"
{if condition="$has_poster"}
      poster="{$poster/}"
{/if}
      {literal}data-setup='{}'>{/literal}
{foreach from="$file_list" item="file"}
    <source src="{$file.address/}{if condition="$has_time"}#t={/if}{if condition="$has_time_start"}{$time_start/}{/if}{if condition="$has_time_stop"},{$time_stop/}{/if}" type='{$file.format/}'>
{/foreach}
    <p>Impossible de lire la video.</p>
  </video>

{foreach from="$link_list" item="link"}
  <p>{$link.title/}: <a href="{$link.address/}">{$link.address/}</a>.</p>
{/foreach}

Besoin d'aide? Contactez votre enseignants par <a href="mailto:{$user.email/}">courriel</a>{if condition="$has_hangout"} ou par Google Hangout:
{literal}
<script src="https://apis.google.com/js/platform.js"></script>
<div id="placeholder-div2"></div>
<script>
  gapi.hangout.render('placeholder-div2', {
    'render': 'createhangout',
    'initial_apps': [{'app_id' : '184219133185', 'start_data' : 'dQw4w9WgXcQ', 'app_type' : 'ROOM_APP' }],
    'widget_size': 72,
	'invites':[{ 'id' : '{/literal}{$user.email/}{literal}', invite_type : 'EMAIL' }]
  });
</script>
{/literal}
{/if}{if condition="$has_skype"} ou par Skype:
{literal}
<script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
<div id="SkypeButton_Call"><script type="text/javascript">
	Skype.ui({
{/literal}
      "name": "dropdown",
      "element": "SkypeButton_Call",
      "participants": ["{$user.skype_account/}"],
      "imageSize": 32
{literal}
    });
  </script></div>
{/literal}
{/if}
{literal}
  <script>
    vid = document.getElementById("vid1");
  </script>
  <script>	
    	videojs("vid1").ready(function(){
        this.videolink();{/literal}
{foreach from="$link_list" item="link"}
        {literal}this.addVideoLink({{/literal}
            caption: "{$link.title/}",
            link: "{$link.address/}",
            size: {$link.size/},
            start: {$link.from_second/},
            end: {$link.to_second/},
	    new_window: true,
	    pause_on_click: true
        {literal}});{/literal}
{/foreach}
    {literal}});{/literal}
  </script>



</body>
</html>
