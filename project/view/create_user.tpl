{include file="view/header.tpl"}{assign name="title" value="Cr�ation d'un utilisateur" /}{/include}
{include file="view/menu.tpl"}{/include}
	<div class="container">
		<div class="content_with_menu">
			<H1>Cr�ation d'un utilisateur</H1>

			<div class="container">
			{if condition="$username_error"}
				<div class="alert-danger" role="alert">
					Le nom d'usager n'est pas valide.
				</div>
			{/if}
			{if condition="$password_error"}
				<div class="alert-danger" role="alert">
					Le mot de passe n'est pas valide.
				</div>
			{/if}
			{if condition="$unknown_error"}
				<div class="alert-danger" role="alert">
					Un erreur non g�r� est survenue.
				</div>
			{/if}

			<form method="post" action="{$script_url/}/user/create/">
				<table class="form-table">
					<tr>
						<td>
							Nom d'usager:
						</td>
						<td>
							<input type="text" name="username" value="{$user.username/}">
						</td>
					</tr>
					<tr>
						<td>
							Mot de passe:
						</td>
						<td>
							<input type="password" name="password" value="">
						</td>
					</tr>
					<tr>
						<td>
							Comfirmation du mot de passe::
						</td>
						<td>
							<input type="password" name="password_confirmation" value="">
						</td>
					</tr>
					<tr>
						<td>
							Pr�nom:
						</td>
						<td>
							<input type="text" name="first_name" value="{$user.first_name/}">
						</td>
					</tr>
					<tr>
						<td>
							Nom de famille:
						</td>
						<td>
							<input type="text" name="last_name" value="{$user.last_name/}">
						</td>
					</tr>
					<tr>
						<td>
							Adresse courriel:
						</td>
						<td>
							<input type="text" name="email" value="{$user.email/}">
						</td>
					</tr>
					<tr>
						<td>
							Nom de compte Skype (Rien inscrire si vous en avez pas):
						</td>
						<td>
							<input type="text" name="skype_account" value="{$user.skype_account/}">	
						</td>
					</tr>
					<tr>
						<td>
							Accessible via Google Hangout:
						</td>
						<td>
							<input type="checkbox" name="has_hangout" {if condition="$has_hangout"} checked {/if} value="1">
						</td>
					</tr>
				</table>
				<input type="submit" class="btn btn-lg btn-default" name="apply" value="Cr�er">
				<input type="submit" class="btn btn-lg btn-default" name="cancel" value="Annuler">
			</form>
		</div>
	</div>
{include file="view/footer.tpl"/}
