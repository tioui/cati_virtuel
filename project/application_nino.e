note
	description: "The system root class when using Nino server connector"
	author: "Louis Marchand"
	date: "The system root class"
	revision: "1.0"

class
	APPLICATION_NINO

inherit
	APPLICATION

create
	make_and_launch

feature {NONE} -- Implementation

	serveur_ini_name:READABLE_STRING_GENERAL
			-- The file name of the ini file contining server options
		do
			Result := "nino.ini"
		end

end
