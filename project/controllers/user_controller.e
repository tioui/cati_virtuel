note
	description: "Manage all {USER} web request"
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	USER_CONTROLLER

inherit
	CONTROLLER
		redefine
			execute,
			default_create
		end

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {CONTROLLER}
			response_method_map.put ([agent list_get, Void], "")
			response_method_map.put ([agent list_get, Void], "list")
			response_method_map.put ([agent create_get, agent create_post], "create")
			response_method_map.put ([agent edit_get, agent edit_post], "edit")
			response_method_map.put ([agent remove_get, Void], "remove")
			response_method_map.put ([agent login_get, agent login_post], "login")
			response_method_map.put ([agent logout_get, Void], "logout")
		end

feature -- Access

	execute (a_request: WSF_REQUEST; res: WSF_RESPONSE)
			-- <Precursor>
		local
			l_message:WSF_RESPONSE_MESSAGE
		do
			-- Todo: response is not pure: modify `new_login_user'
			new_login_user := Void
			l_message := response (a_request)
			if attached request_type(a_request) as la_type then
				if la_type.is_equal("login") and attached new_login_user as l_user then
					set_login_user (a_request, res, l_user)
				elseif la_type.is_equal("logout") then
					logout_user(a_request, res)
				end
			end
			res.send (l_message)
		end

feature {NONE} -- Implementation

	new_login_user:detachable USER
			-- When a `login_post' has been called and the login succeeded, this object is
			-- set to the new logged {USER}

	logout_get(a_request: WSF_REQUEST): WSF_REDIRECTION_RESPONSE
			-- Page to show when `a_request' is asking the GET method of a logout `request_type'.
			-- Remove every session cookie in the client broswer
		do
			create Result.make(a_request.script_url ("/"))
		end

	list_get(a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a list `request_type'.
			-- List every {USER} in the system.
		local
			l_template:TEMPLATE_FILE
			l_users:LINKED_LIST[USER_VIDEO_LIST_VIEW_MODEL]
		do
			if attached login_user(a_request) then
				create l_template.make_from_file ("view/list_user.tpl")
				initialize_template(l_template, a_request)
				user_repository.fetch_all
				create l_users.make
				if attached user_repository.items as la_fetched_users then
					across la_fetched_users  as la_users loop
						l_users.extend (create {USER_VIDEO_LIST_VIEW_MODEL}.make (la_users.item, 0))
					end
					l_template.add_value (l_users, "user_list")
				end
				create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	login_get (a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a login `request_type'.
		do
			if not attached login_user(a_request) then
				Result := login_get_with_error(a_request, False)
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/"))
			end
		end

	login_get_with_error (a_request: WSF_REQUEST; a_error:BOOLEAN): HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the GET method of a login `request_type' using
			-- `a_error' to know if an error has happend.
		local
			l_template:TEMPLATE_FILE
		do
			create l_template.make_from_file ("view/user_login.tpl")
			initialize_template(l_template, a_request)
			if a_error then
				l_template.add_value (True, "has_error")
			end
			create Result.make (l_template)
		end

	login_post (a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of a login `request_type'.
			-- If the login form fields are valid, set the `new_login_user' to the new logged {USER}
		local
			l_username, l_password: READABLE_STRING_GENERAL
		do
			if attached a_request.form_parameter ("username") as la_username then
				l_username := la_username.string_representation
				if attached a_request.form_parameter ("password") as la_password then
					l_password := la_password.string_representation
					user_repository.fetch_by_username_and_password (l_username, l_password)
					if attached user_repository.item as la_user then
						create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/video/list/"))
						new_login_user := la_user
					else
						Result := login_get_with_error(a_request, True)
					end
				else
					Result := login_get_with_error(a_request, True)
				end
			else
				Result := login_get_with_error(a_request, True)
			end
		end

	create_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a create `request_type'.
		do
			if attached login_user(a_request) then
				Result := show_user(a_request, "view/create_user.tpl", create {USER}, False, False, False)
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	create_post(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of a create `request_type'.
			-- If the create form fields are valid, create a {USER} and save it in the database.
		local
			l_user:USER
			l_username_error, l_password_error, l_unknown_error:BOOLEAN
			l_template:TEMPLATE_FILE
		do
			if attached login_user(a_request) then
				if attached a_request.form_parameter ("cancel") then
					create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/list/"))
				else
					if attached {USER} object_from_form (a_request, user_repository.prototype, "", "") as la_user then
						l_username_error := False
						if la_user.username.is_empty then
							l_username_error := True
						else
							user_repository.fetch_by_username (la_user.username)
							if attached user_repository.item then
								l_username_error := True
							end
						end
						l_password_error := False
						if la_user.password.is_empty then
							l_password_error := True
						else
							if not (attached a_request.form_parameter ("password_confirmation") as la_password_confirmation and then
									la_user.password.same_string (la_password_confirmation.string_representation)) then
								l_password_error := True
							end
						end
						l_user := la_user
					else
						l_user := user_repository.create_new
						l_unknown_error := True
					end
					if l_unknown_error or l_username_error or l_password_error then
						Result := show_user(a_request, "view/create_user.tpl", l_user, l_username_error, l_password_error, l_unknown_error)
					else
						l_user.save
						create l_template.make_from_file ("view/confirm_create_user.tpl")
						initialize_template(l_template, a_request)
						l_template.add_value (l_user, "user")
						create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
					end
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	edit_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a edit `request_type'.
		do
			if attached login_user(a_request) then
				if attached a_request.path_parameter ("user_id") as la_user_id and then la_user_id.string_representation.is_integer then
					user_repository.fetch_by_id (la_user_id.string_representation.to_integer)
					if attached user_repository.item as la_user then
						Result := show_user(a_request, "view/edit_user.tpl", la_user, False, False, False)
					else
						Result := object_not_found(a_request)
					end
				else
					Result := argument_not_found_response(a_request)
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	edit_post(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of a edit `request_type'.
			-- If the edit form fields are valid, modify the {USER} and save it in the database.
		local
			l_username_error, l_password_error, l_unknown_error:BOOLEAN
			l_template:TEMPLATE_FILE
		do
			if attached login_user(a_request) then
				if attached a_request.form_parameter ("cancel") then
					create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/list/"))
				else
					if attached {USER} object_from_form (a_request, user_repository.prototype, "", "") as la_user then
						l_username_error := False
						if la_user.username.is_empty then
							l_username_error := True
						else
							user_repository.fetch_by_username (la_user.username)
							if attached user_repository.item as la_other_user then
								if la_other_user.id /= la_user.id then
									l_username_error := True
								end
							end
						end
						l_password_error := False
						if la_user.password.is_empty then
							user_repository.fetch_by_id (la_user.id)
							if attached user_repository.item as la_other_user then
								la_user.set_password (la_other_user.password)
							else
								l_password_error := True
							end
						else
							if not (attached a_request.form_parameter ("password_confirmation") as la_password_confirmation and then
									la_user.password.same_string (la_password_confirmation.string_representation)) then
								l_password_error := True
							end
						end
						if l_unknown_error or l_username_error or l_password_error then
							Result := show_user(a_request, "view/edit_user.tpl", la_user, l_username_error, l_password_error, l_unknown_error)
						else
							la_user.save
							create l_template.make_from_file ("view/confirm_edit_user.tpl")
							initialize_template(l_template, a_request)
							l_template.add_value (la_user, "user")
							create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
						end

					else
						result := unmanaged_error (a_request)
					end
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	remove_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a remove `request_type'.
			-- Remove the selected {USER} from the database
		local
			l_template:TEMPLATE_FILE
			l_id:INTEGER
		do
			if attached login_user(a_request) as la_login_user then
				if attached a_request.path_parameter ("user_id") as la_user_id and then la_user_id.string_representation.is_integer then
					user_repository.fetch_by_id (la_user_id.string_representation.to_integer)
					if attached user_repository.item as la_user then
						create l_template.make_from_file ("view/confirm_remove_user.tpl")
						initialize_template(l_template, a_request)
						l_id := la_user.id
						l_template.add_value (la_user, "user")
						if la_user.id = la_login_user.id then
							l_template.add_value (True, "same_user")
							l_template.add_value (True, "error")
						else
							la_user.delete
							user_repository.fetch_by_id (l_id)
							if attached user_repository.item then
								l_template.add_value (True, "error")
							end
						end
						create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
					else
						Result := object_not_found(a_request)
					end
				else
					Result := argument_not_found_response(a_request)
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	show_user(
			a_request: WSF_REQUEST;
			a_template_name:READABLE_STRING_GENERAL;
			a_user:USER;
			a_username_error, a_password_error, a_unknown_error:BOOLEAN
		):HTML_TEMPLATE_PAGE_RESPONSE
			-- Show `a_user' fields in the view `a_template_name'. Use `a_username_error',
			-- `a_password_error' and `a_unknown_error' to know if errors occured.
		local
			l_template:TEMPLATE_FILE
		do
			create l_template.make_from_file (a_template_name)
			initialize_template(l_template, a_request)
			l_template.add_value (a_user, "user")
			if a_username_error then
				l_template.add_value (True, "username_error")
			end
			if a_password_error then
				l_template.add_value (True, "password_error")
			end
			if a_unknown_error then
				l_template.add_value (True, "unknown_error")
			end
			if a_user.has_hangout_account then
				l_template.add_value (True, "has_hangout")
			end
			create Result.make (l_template)
		end

end
