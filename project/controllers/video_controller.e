note
	description: "Manage all {VIDEO} web request"
	author: "Louis Marchand"
	date: "2014, September 29"
	revision: "1.0"

class
	VIDEO_CONTROLLER

inherit
	CONTROLLER
		redefine
			default_create
		end

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {CONTROLLER}
			response_method_map.put ([agent list_get, Void], "")
			response_method_map.put ([agent list_get, agent list_post], "list")
			response_method_map.put ([agent create_get, agent create_post], "create")
			response_method_map.put ([agent edit_get, agent edit_post], "edit")
			response_method_map.put ([agent remove_get, Void], "remove")
			response_method_map.put ([agent view_get, Void], "view")
			response_method_map.put ([agent address_get, agent address_post], "address")

		end

feature {NONE} -- Implementation

	list_post(a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of a list `request_type'.
			-- Show the {VIDEO} list of another {USER}.
		do
			if attached login_user(a_request) then
				if attached a_request.form_parameter ("user_id") as la_user_id then
					create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/video/list/" + la_user_id.string_representation + "/"))
				else
					Result := list_get(a_request)
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	list_get (a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a list `request_type'.
		local
			l_template:TEMPLATE_FILE
			l_user:USER
			l_user_list:ARRAYED_LIST[USER_VIDEO_LIST_VIEW_MODEL]
		do
			if attached login_user(a_request) as la_user then
				create l_template.make_from_file ("view/list_video.tpl")
				initialize_template(l_template, a_request)
				l_user := la_user
				if attached a_request.path_parameter ("video_id") as la_id then
					if la_id.string_representation.is_integer then
						user_repository.fetch_by_id (la_id.string_representation.to_integer)
						if attached user_repository.item as la_fetched_user then
							l_user := la_fetched_user
						end
					end
				end
				user_repository.fetch_all
				if attached user_repository.items as la_users then
					create l_user_list.make (la_users.count)
					across
						la_users as la_users_list
					loop
						l_user_list.extend (create{USER_VIDEO_LIST_VIEW_MODEL}.make (la_users_list.item,
																					l_user.id))
					end

					l_template.add_value (l_user_list, "user_list")
				end
				l_template.add_value (la_user, "current_user")
				l_template.add_value (l_user.videos, "video_list")
				create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	create_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a create `request_type'.
		do
			Result := create_get_with_error(a_request, False)
		end

	create_post(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of a create `request_type'.
			-- This can be to add or remove a {VIDEO_FILE} or a {LINK}, modify the {VIDEO} or cancel
			-- the operation.
		do
			if attached login_user(a_request) as la_user then
				if attached a_request.form_parameter ("create") then
					Result := create_post_create(a_request, la_user)
				elseif attached a_request.form_parameter ("add_file") then
					Result := edit_post_add_file("view/create_video.tpl", a_request)
				elseif attached a_request.form_parameter ("add_link") then
					Result := edit_post_add_link("view/create_video.tpl", a_request)
				elseif attached a_request.form_parameter ("cancel") then
					create {WSF_REDIRECTION_RESPONSE} Result.make ("/video/list/")
				else
					if attached edit_post_remove_file("view/create_video.tpl", a_request) as la_result then
						Result := la_result
					elseif attached edit_post_remove_link("view/create_video.tpl", a_request) as la_result then
						Result := la_result
					else
						Result := create_get_with_error(a_request, True)
					end
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	edit_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of an edit `request_type'.
		do
			Result := edit_get_with_error(a_request, False)
		end

	edit_post(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of an edit `request_type'.
			-- This can be to add or remove a {VIDEO_FILE} or a {LINK}, modify the {VIDEO} or cancel
			-- the operation.
		do
			if attached login_user(a_request) as la_user then
				if attached a_request.form_parameter ("modify") then
					Result := edit_post_modify(a_request, la_user)
				elseif attached a_request.form_parameter ("add_file") then
					Result := edit_post_add_file("view/edit_video.tpl", a_request)
				elseif attached a_request.form_parameter ("add_link") then
					Result := edit_post_add_link("view/edit_video.tpl", a_request)
				elseif attached a_request.form_parameter ("cancel") then
					create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/video/list/"))
				elseif attached a_request.form_parameter ("reset") then
					Result := edit_get_with_error(a_request, False)
				else
					if attached edit_post_remove_file("view/edit_video.tpl", a_request) as la_result then
						Result := la_result
					elseif attached edit_post_remove_link("view/edit_video.tpl", a_request) as la_result then
						Result := la_result
					else
						Result := edit_get_with_error(a_request, True)
					end
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	view_get (a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of an view `request_type'.
		local
			l_template:TEMPLATE_FILE
			l_creator:USER
		do
			if attached a_request.path_parameter ("video_id") as la_video_id and then
					la_video_id.string_representation.is_integer then
				video_repository.fetch_by_id (la_video_id.string_representation.to_integer)
				if attached video_repository.item as la_video then
					create l_template.make_from_file ("view/view_video.tpl")
					initialize_template(l_template, a_request)
					l_template.add_value (la_video, "video")
					if attached la_video.poster as la_poster then
						l_template.add_value (True, "has_poster")
						l_template.add_value (la_poster, "poster")
					end
					l_creator := la_video.creator
					l_template.add_value (la_video.creator, "user")
					l_template.add_value (la_video.links, "link_list")
					l_template.add_value (la_video.files, "file_list")
					if attached login_user(a_request) as la_user then
						l_template.add_value (True, "user_connect")
					end
					if la_video.creator.has_hangout_account then
						l_template.add_value (True, "has_hangout")
					end
					if not l_creator.skype_account.is_empty then
						l_template.add_value (True, "has_skype")
					end
					if attached a_request.query_parameter ("autoplay") then
						l_template.add_value (True, "autoplay")
					end
					if attached a_request.query_parameter ("start") as la_start and then la_start.string_representation.is_integer then
						l_template.add_value (True, "has_time")
						l_template.add_value (True, "has_time_start")
						l_template.add_value (la_start.string_representation.to_integer, "time_start")
					end
					if attached a_request.query_parameter ("stop") as la_start and then la_start.string_representation.is_integer then
						l_template.add_value (True, "has_time")
						l_template.add_value (True, "has_time_stop")
						l_template.add_value (la_start.string_representation.to_integer, "time_stop")
					end
					create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
				else
					Result := object_not_found(a_request)
				end
			else
				Result := argument_not_found_response(a_request)
			end
		end

	address_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of an address `request_type'.
		local
			l_template:TEMPLATE_FILE
			l_address:STRING_GENERAL
		do
			if attached login_user(a_request) as la_user then
				if
					attached a_request.path_parameter ("video_id") as la_video_id and then
					la_video_id.string_representation.is_integer
				then
					video_repository.fetch_by_id (la_video_id.string_representation.to_integer)
					if attached video_repository.item as la_video then
						create l_template.make_from_file ("view/address_video.tpl")
						initialize_template(l_template, a_request)
						l_template.add_value (la_video.id, "video_id")
						l_template.add_value ("", "start_time")
						l_template.add_value ("", "stop_time")
						l_address := a_request.absolute_script_url ("/video/view/" + la_video.id.out)
						l_template.add_value (l_address, "address")
						create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
					else
						Result := object_not_found(a_request)
					end
				else
					Result := argument_not_found_response(a_request)
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	address_post(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the POST method of an address `request_type'.
			-- Adjust the arguments of the query to show the video with options
		local
			l_template:TEMPLATE_FILE
			l_address:STRING_GENERAL
			l_is_first:BOOLEAN
		do
			if attached login_user(a_request) as la_user then
				if
					attached a_request.path_parameter ("video_id") as la_video_id and then
					la_video_id.string_representation.is_integer
				then
					video_repository.fetch_by_id (la_video_id.string_representation.to_integer)
					if attached video_repository.item as la_video then
						create l_template.make_from_file ("view/address_video.tpl")
						initialize_template(l_template, a_request)
						l_template.add_value (la_video.id, "video_id")
						l_address := a_request.absolute_script_url ("/video/view/" + la_video.id.out)
						l_is_first := True
						if attached a_request.form_parameter ("is_autoplay") then
							if l_is_first then
								l_is_first := False
								l_address.append ("?")
							else
								l_address.append ("&")
							end
							l_address.append ("autoplay")
							l_template.add_value (True, "is_autoplay")
						end
						if attached a_request.form_parameter ("start_time") as la_start_time and then la_start_time.string_representation.is_integer then
							if l_is_first then
								l_is_first := False
								l_address.append ("?")
							else
								l_address.append ("&")
							end
							l_address.append ("start=" + la_start_time.string_representation)
							l_template.add_value (la_start_time.string_representation, "start_time")
						else
							l_template.add_value ("", "start_time")
						end
						if attached a_request.form_parameter ("stop_time") as la_stop_time and then la_stop_time.string_representation.is_integer then
							if l_is_first then
								l_is_first := False
								l_address.append ("?")
							else
								l_address.append ("&")
							end
							l_address.append ("stop=" + la_stop_time.string_representation)
							l_template.add_value (la_stop_time.string_representation, "stop_time")
						else
							l_template.add_value ("", "stop_time")
						end
						l_template.add_value (l_address, "address")
						create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
					else
						Result := object_not_found(a_request)
					end
				else
					Result := argument_not_found_response(a_request)
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	remove_get(a_request: WSF_REQUEST):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a remove `request_type'.
			-- Remove the selected {VIDEO} from the database and every containing `files_and_poste' and `links'
		local
			l_template:TEMPLATE_FILE
			l_files:LIST[VIDEO_FILE]
			l_links:LIST[LINK]
			l_old_id:INTEGER
		do
			if attached login_user(a_request) then
				if
					attached a_request.path_parameter ("video_id") as la_video_id and then
					la_video_id.string_representation.is_integer
				then
					video_repository.fetch_by_id (la_video_id.string_representation.to_integer)
					if attached video_repository.item as la_video then
						l_files := la_video.files_and_poster
						l_files.do_all (agent (a_file:VIDEO_FILE) do a_file.delete end)
						l_links := la_video.links
						l_links.do_all (agent (a_link:LINK) do a_link.delete end)
						l_old_id := la_video.id
						la_video.delete
						create l_template.make_from_file ("view/confirm_remove_video.tpl")
						initialize_template(l_template, a_request)
						l_template.add_value (la_video, "video")
						video_repository.fetch_by_id (l_old_id)
						if attached video_repository.item then
							l_template.add_value (True, "error")
						end
						create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
					else
						Result := object_not_found(a_request)
					end
				else
					Result := argument_not_found_response(a_request)
				end
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end


	create_get_with_error(a_request: WSF_REQUEST; a_error:BOOLEAN):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of a remove `request_type' using
			-- `a_error' to know if an error has happend.
		local
			l_template:TEMPLATE_FILE
			l_files:ARRAYED_LIST[VIDEO_FILE]
			l_video:VIDEO_VIDEO_EDIT_VIEW_MODEL
		do
			if attached login_user(a_request) then
				create l_template.make_from_file ("view/create_video.tpl")
				initialize_template(l_template, a_request)
				create l_video.make (retreive_main_fields(a_request))
				l_template.add_value (l_video, "video")
				l_template.add_value (1, "nb_file")
				l_template.add_value (0, "nb_link")
				create l_files.make (1)
				l_files.extend (create {FILE_VIDEO_EDIT_VIEW_MODEL}.make (create {VIDEO_FILE}, 1))
				l_template.add_value (l_files, "file_list")
				if a_error then
					l_template.add_value (True, "has_error")
				end
				create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
			else
				create {WSF_REDIRECTION_RESPONSE} Result.make (a_request.script_url ("/user/login/"))
			end
		end

	create_post_create(a_request: WSF_REQUEST; a_user:USER):HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the POST method of a create `request_type'.
			-- If the form fields are valid, create a new {VIDEO} using `a_user' as the creator and store
			-- it in the database.
		local
			l_video:VIDEO
			l_link_list:LIST[LINK]
			l_file_list:LIST[VIDEO_FILE]
			l_template:TEMPLATE_FILE
			l_address:STRING_GENERAL
		do
			l_video := retreive_main_fields(a_request)
			l_video.set_creator (a_user)
			l_link_list := retreive_links(a_request)
			l_file_list := retreive_files(a_request)
			l_video.save
			across l_link_list as la_links loop
				la_links.item.set_video (l_video)
				la_links.item.save
			end
			across l_file_list as la_files loop
				la_files.item.set_video (l_video)
				la_files.item.save
			end
			create l_template.make_from_file ("view/confirm_create_video.tpl")
			initialize_template(l_template, a_request)
			l_template.add_value (l_video.id, "video_id")
			l_template.add_value ("", "start_time")
			l_template.add_value ("", "stop_time")
			l_address := a_request.absolute_script_url ("/video/view/" + l_video.id.out)
			l_template.add_value (l_address, "address")
			create Result.make (l_template)
		end

	edit_post_modify(a_request: WSF_REQUEST; a_user:USER):HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the POST method of a edit `request_type'.
			-- If the form fields are valid, modify the requested {VIDEO} using `a_user' as the creator
			-- if there is no creator in the original object and store it in the database
		local
			l_video:VIDEO
			l_link_list:LIST[LINK]
			l_file_list:LIST[VIDEO_FILE]
			l_remove_model:LIST[MODEL]
			l_template:TEMPLATE_FILE
			l_address:STRING_GENERAL
		do
			l_video := retreive_main_fields(a_request)
			if l_video.user_id = 0 then
				l_video.set_creator (a_user)
			end
			l_link_list := retreive_links(a_request)
			l_remove_model := difference_lists_model(l_video.links, l_link_list)
			l_remove_model.do_all (agent (a_model:MODEL) do a_model.delete end)

			l_file_list := retreive_files(a_request)
			l_remove_model := difference_lists_model(l_video.files_and_poster, l_file_list)
			l_remove_model.do_all (agent (a_model:MODEL) do a_model.delete end)

			l_video.save
			across l_link_list as la_links loop
				la_links.item.set_video (l_video)
				la_links.item.save
			end
			across l_file_list as la_files loop
				la_files.item.set_video (l_video)
				la_files.item.save
			end
			create l_template.make_from_file ("view/confirm_edit_video.tpl")
			initialize_template(l_template, a_request)
			l_template.add_value (l_video.id, "video_id")
			l_template.add_value ("", "start_time")
			l_template.add_value ("", "stop_time")
			l_address := a_request.absolute_script_url ("/video/view/" + l_video.id.out)
			l_template.add_value (l_address, "address")
			create Result.make (l_template)
		end

	difference_lists_model(a_list_1, a_list_2:LINEAR[MODEL]):LIST[MODEL]
			-- Every elements of `a_list_1' that is not in `a_list_2'
		local
			l_is_found:BOOLEAN
		do
			create {LINKED_LIST[MODEL]}Result.make
			from
				a_list_1.start
			until
				a_list_1.exhausted
			loop
				from
					a_list_2.start
					l_is_found := False
				until
					a_list_2.exhausted or
					l_is_found
				loop
					if a_list_1.item.id = a_list_2.item.id then
						l_is_found := True
					end
					a_list_2.forth
				end
				if not l_is_found then
					Result.extend (a_list_1.item)
				end
				a_list_1.forth
			end
		end

	edit_post_add_file(a_view_file: READABLE_STRING_GENERAL; a_request: WSF_REQUEST):HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the POST method of a edit and create `request_type'.
			-- Add a {VIDEO_FILE} to the `files_and_poster' of the selected {VIDEO} and return a page generate from
			-- `a_view_file'.
		local
			l_template:TEMPLATE_FILE
			l_link_list:LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			l_file_list:LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			l_file:VIDEO_FILE
			l_video:VIDEO_VIDEO_EDIT_VIEW_MODEL
		do
			create l_template.make_from_file (a_view_file)
			initialize_template(l_template, a_request)
			create l_video.make(retreive_main_fields(a_request))
			l_template.add_value (l_video, "video")
			l_file_list := retreive_files_view_model(a_request)
			create l_file
			l_file.set_file_order(0)
			l_file_list.extend (create {FILE_VIDEO_EDIT_VIEW_MODEL}.make (l_file, l_file_list.count + 1))
			l_template.add_value (l_file_list.count, "nb_file")
			l_template.add_value (l_file_list, "file_list")
			l_link_list := retreive_links_view_model(a_request)
			l_template.add_value (l_link_list, "link_list")
			l_template.add_value (l_link_list.count, "nb_link")
			create Result.make (l_template)
		end

	edit_post_remove_file(a_view_file: READABLE_STRING_GENERAL; a_request: WSF_REQUEST):detachable HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the POST method of a create and edit `request_type'.
			-- Remove a {VIDEO_FILE} of the `files_and_poster' of the selected {VIDEO} and return a page generate from
			-- `a_view_file'.
		local
			l_template:TEMPLATE_FILE
			l_file_list, l_new_file_list:LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			l_link_list:LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			l_nb_file, l_count, l_remove_index:INTEGER
			l_video:VIDEO_VIDEO_EDIT_VIEW_MODEL
		do
			if
				attached a_request.form_parameter ("nb_file") as la_nb_file and then
				la_nb_file.string_representation.is_integer
			then
				l_nb_file := la_nb_file.string_representation.to_integer
				from
					l_count := 1
					l_remove_index := 0
				until
					l_count > l_nb_file or
					l_remove_index > 0
				loop
					if attached a_request.form_parameter ("file_remove" + l_count.out) then
						l_remove_index := l_count
					end
					l_count := l_count + 1
				end
				if l_remove_index > 0 then
					create l_template.make_from_file (a_view_file)
					initialize_template(l_template, a_request)
					create l_video.make (retreive_main_fields(a_request))
					l_template.add_value (l_video, "video")
					l_file_list := retreive_files_view_model(a_request)
					create {ARRAYED_LIST[FILE_VIDEO_EDIT_VIEW_MODEL]}l_new_file_list.make(l_file_list.count - 1)
					l_count := 1
					across l_file_list as la_file_list loop
						if la_file_list.item.no /= l_remove_index then
							la_file_list.item.set_no(l_count)
							l_new_file_list.extend (la_file_list.item)
							l_count := l_count + 1
						end
					end
					l_template.add_value (l_new_file_list.count, "nb_file")
					l_template.add_value (l_new_file_list, "file_list")
					l_link_list := retreive_links_view_model(a_request)
					l_template.add_value (l_link_list, "link_list")
					l_template.add_value (l_link_list.count, "nb_link")
					create Result.make (l_template)
				end
			end
		end

	edit_post_remove_link(a_view_file: READABLE_STRING_GENERAL; a_request: WSF_REQUEST):detachable HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the POST method of a create and edit `request_type'.
			-- Remove a {LINK} of the `links' of the selected {VIDEO} and return a page generate from
			-- `a_view_file'.
		local
			l_template:TEMPLATE_FILE
			l_file_list:LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			l_link_list, l_new_link_list:LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			l_nb_link, l_count, l_remove_index:INTEGER
			l_video:VIDEO_VIDEO_EDIT_VIEW_MODEL
		do
			if
				attached a_request.form_parameter ("nb_link") as la_nb_link and then
				la_nb_link.string_representation.is_integer
			then
				l_nb_link := la_nb_link.string_representation.to_integer
				from
					l_count := 1
					l_remove_index := 0
				until
					l_count > l_nb_link or
					l_remove_index > 0
				loop
					if attached a_request.form_parameter ("link_remove" + l_count.out) then
						l_remove_index := l_count
					end
					l_count := l_count + 1
				end
				if l_remove_index > 0 then
					create l_template.make_from_file (a_view_file)
					initialize_template(l_template, a_request)
					create l_video.make (retreive_main_fields(a_request))
					l_template.add_value (l_video, "video")
					l_file_list := retreive_files_view_model(a_request)
					l_template.add_value (l_file_list.count, "nb_file")
					l_template.add_value (l_file_list, "file_list")
					l_link_list := retreive_links_view_model(a_request)
					create {ARRAYED_LIST[LINK_VIDEO_EDIT_VIEW_MODEL]}l_new_link_list.make(l_link_list.count - 1)
					l_count := 1
					across l_link_list as la_link_list loop
						if la_link_list.item.no /= l_remove_index then
							la_link_list.item.set_no(l_count)
							l_new_link_list.extend (la_link_list.item)
							l_count := l_count + 1
						end
					end
					l_template.add_value (l_new_link_list, "link_list")
					l_template.add_value (l_new_link_list.count, "nb_link")
					create Result.make (l_template)
				end
			end
		end

	edit_post_add_link(a_view_file: READABLE_STRING_GENERAL; a_request: WSF_REQUEST):HTML_TEMPLATE_PAGE_RESPONSE
			-- Page to show when `a_request' is asking the POST method of a edit and create `request_type'.
			-- Add a {LINK} to the `links' of the selected {VIDEO} and return a page generate from
			-- `a_view_file'.
		local
			l_template:TEMPLATE_FILE
			l_link_list:LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			l_file_list:LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			l_link:LINK
			l_video:VIDEO_VIDEO_EDIT_VIEW_MODEL
		do
			create l_template.make_from_file (a_view_file)
			initialize_template(l_template, a_request)
			create l_video.make (retreive_main_fields(a_request))
			l_template.add_value (l_video, "video")
			l_file_list := retreive_files_view_model(a_request)
			l_template.add_value (l_file_list.count, "nb_file")
			l_template.add_value (l_file_list, "file_list")
			l_link_list := retreive_links_view_model(a_request)
			create l_link
			l_link.set_size (12)
			l_link.set_from_second (0)
			l_link.set_to_second (1)
			l_link_list.extend (create {LINK_VIDEO_EDIT_VIEW_MODEL}.make (l_link, l_link_list.count + 1))
			l_template.add_value (l_link_list, "link_list")
			l_template.add_value (l_link_list.count, "nb_link")
			create Result.make (l_template)
		end

	edit_get_with_error(a_request: WSF_REQUEST; a_error:BOOLEAN):WSF_RESPONSE_MESSAGE
			-- Page to show when `a_request' is asking the GET method of an edit `request_type' using
			-- `a_error' to know if an error has happend.
		local
			l_template:TEMPLATE_FILE
			l_link_list:LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			l_file_list:LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			l_video_files:LIST[VIDEO_FILE]
			l_video_links:LIST[LINK]
			l_count:INTEGER
		do
			if
				attached a_request.path_parameter ("video_id") as la_video_id and then
				la_video_id.string_representation.is_integer
			then
				video_repository.fetch_by_id (la_video_id.string_representation.to_integer)
				if attached video_repository.item as la_video then
					create l_template.make_from_file ("view/edit_video.tpl")
					initialize_template(l_template, a_request)
					l_template.add_value (la_video, "video")
					l_video_files := la_video.files_and_poster
					create {ARRAYED_LIST[FILE_VIDEO_EDIT_VIEW_MODEL]}l_file_list.make (l_video_files.count)
					l_count := 1
					across l_video_files as la_video_files loop
						l_file_list.extend (create {FILE_VIDEO_EDIT_VIEW_MODEL}.make (la_video_files.item, l_count))
						l_count := l_count + 1
					end
					l_template.add_value (l_file_list.count, "nb_file")
					l_template.add_value (l_file_list, "file_list")
					l_video_links := la_video.links
					create {ARRAYED_LIST[LINK_VIDEO_EDIT_VIEW_MODEL]}l_link_list.make (l_video_links.count)
					l_count := 1
					across l_video_links as la_video_links loop
						l_link_list.extend (create {LINK_VIDEO_EDIT_VIEW_MODEL}.make (la_video_links.item, l_count))
						l_count := l_count + 1
					end
					l_template.add_value (l_link_list.count, "nb_link")
					l_template.add_value (l_link_list, "link_list")
					if a_error then
						l_template.add_value (True, "error")
					end
					create {HTML_TEMPLATE_PAGE_RESPONSE}Result.make (l_template)
				else
					Result := object_not_found(a_request)
				end
			else
				Result := argument_not_found_response(a_request)
			end
		end

	retreive_main_fields(a_request: WSF_REQUEST):VIDEO
			-- Retreive the {VIDEO} object from the form fields of `a_request'
		do
			if attached {VIDEO}object_from_form(a_request, video_repository.prototype, "", "") as la_object then
				Result := 	la_object
			else
				create Result
				Result.set_height (480)
				Result.set_width (640)
			end
		end

	retreive_links(a_request: WSF_REQUEST):LIST[LINK]
			-- Retreive every {LINK} object from the form fields of `a_request'
		local
			l_list:ARRAYED_LIST[LINK]
			l_list_view_model:LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
		do
			l_list_view_model := retreive_links_view_model(a_request)
			create l_list.make (l_list_view_model.count)
			across l_list_view_model as la_view_model loop
				l_list.extend (la_view_model.item.model_representation)
			end
			Result := l_list
		end

	retreive_links_view_model(a_request: WSF_REQUEST):LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			-- Retreive the {LINK_VIDEO_EDIT_VIEW_MODEL} object from the form fields of `a_request'
		local
			l_list:ARRAYED_LIST[LINK_VIDEO_EDIT_VIEW_MODEL]
			l_nb_link:INTEGER
		do
			if attached a_request.form_parameter ("nb_link") as la_nb_ligne_value and then
							la_nb_ligne_value.string_representation.is_integer then
				l_nb_link := la_nb_ligne_value.string_representation.to_integer
				create l_list.make (l_nb_link)
				across (1 |..| l_nb_link) as la_no loop
					if attached {LINK_VIDEO_EDIT_VIEW_MODEL}object_from_form(a_request, create {LINK_VIDEO_EDIT_VIEW_MODEL}, "link_",la_no.item.out) as la_link then
						la_link.set_no(la_no.item)
						l_list.extend (la_link)
					end
				end
			else
				create l_list.make (0)
			end
			Result := l_list
		end

	retreive_files(a_request: WSF_REQUEST):LIST[VIDEO_FILE]
			-- Retreive every {VIDEO_FILE} object from the form fields of `a_request'
		local
			l_list:ARRAYED_LIST[VIDEO_FILE]
			l_list_view_model:LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
		do
			l_list_view_model := retreive_files_view_model(a_request)
			create l_list.make (l_list_view_model.count)
			across l_list_view_model as la_view_model loop
				l_list.extend (la_view_model.item.model_representation)
			end
			Result := l_list
		end

	retreive_files_view_model(a_request: WSF_REQUEST):LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			-- Retreive every {FILE_VIDEO_EDIT_VIEW_MODEL} object from the form fields of `a_request'
		local
			l_list:ARRAYED_LIST[FILE_VIDEO_EDIT_VIEW_MODEL]
			l_nb_files:INTEGER
		do
			if attached a_request.form_parameter ("nb_file") as la_nb_ligne_value and then
							la_nb_ligne_value.string_representation.is_integer then
				l_nb_files := la_nb_ligne_value.string_representation.to_integer
				create l_list.make (l_nb_files)
				across (1 |..| l_nb_files) as la_no loop
					if attached {FILE_VIDEO_EDIT_VIEW_MODEL}object_from_form(a_request, create {FILE_VIDEO_EDIT_VIEW_MODEL}, "file_",la_no.item.out) as la_file then
						la_file.set_no(la_no.item)
						l_list.extend (la_file)
					end
				end
			else
				create l_list.make (0)
			end
			Result := l_list
		end

end
