import config
import unittest
import base_test
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

class User(base_test.Base_Test):

    def test_user1_main(self):
        self.login()
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.CLASS_NAME , "table")))
        self.driver.get(config.HostURL + "/user/create")
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("user1")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("pass1")
        password_confirmation_field = self.driver.find_element_by_name("password_confirmation")
        password_confirmation_field.send_keys("pass1")
        first_name_field = self.driver.find_element_by_name("first_name")
        first_name_field.send_keys("User1FirstName")
        last_name_field = self.driver.find_element_by_name("last_name")
        last_name_field.send_keys("User1LastName")
        email_field = self.driver.find_element_by_name("email")
        email_field.send_keys("user1@email.com")
        skype_account_field = self.driver.find_element_by_name("skype_account")
        skype_account_field.send_keys("user1.skype")
        submit_button = self.driver.find_element_by_name("apply")
        submit_button.click()
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.NAME , "success")))
        self.driver.get(config.HostURL + "/user/list")
        usernames = self.driver.find_elements_by_class_name("username_list_field")
        index = len(usernames)-1
        assert usernames[index].text == "user1"
        firstnames = self.driver.find_elements_by_class_name("first_name_list_field")
        assert self.get_inner_text(firstnames[index]) == "User1FirstName"
        lastnames = self.driver.find_elements_by_class_name("last_name_list_field")
        assert self.get_inner_text(lastnames[index]) == "User1LastName"
        emails = self.driver.find_elements_by_class_name("email_list_field")
        assert emails[index].text == "user1@email.com"
        has_hangouts = self.driver.find_elements_by_class_name("has_hangout_list_field")
        assert self.get_inner_text(has_hangouts[index]) == "no"
        skype_accounts = self.driver.find_elements_by_class_name("skype_account_list_field")
        assert skype_accounts[index].text == "user1.skype"
        user_ids = self.driver.find_elements_by_class_name("id_list_field")
        user_id = self.get_inner_text(user_ids[index])
        edit_link = self.driver.find_element_by_name("edit_link" + user_id)
        edit_link.click()
        cancel_button = self.driver.find_element_by_name("cancel")
        cancel_button.click()
        usernames = self.driver.find_elements_by_class_name("username_list_field")
        index = len(usernames)-1
        assert usernames[index].text == "user1"
        firstnames = self.driver.find_elements_by_class_name("first_name_list_field")
        assert self.get_inner_text(firstnames[index]) == "User1FirstName"
        lastnames = self.driver.find_elements_by_class_name("last_name_list_field")
        assert self.get_inner_text(lastnames[index]) == "User1LastName"
        emails = self.driver.find_elements_by_class_name("email_list_field")
        assert emails[index].text == "user1@email.com"
        has_hangouts = self.driver.find_elements_by_class_name("has_hangout_list_field")
        assert self.get_inner_text(has_hangouts[index]) == "no"
        skype_accounts = self.driver.find_elements_by_class_name("skype_account_list_field")
        assert skype_accounts[index].text == "user1.skype"
        edit_link = self.driver.find_element_by_name("edit_link" + user_id)
        edit_link.click()
        username_field = self.driver.find_element_by_name("username")
        username_field.clear()
        username_field.send_keys("user2")
        password_field = self.driver.find_element_by_name("password")
        password_field.clear()
        password_field.send_keys("pass2")
        password_confirmation_field = self.driver.find_element_by_name("password_confirmation")
        password_confirmation_field.clear()
        password_confirmation_field.send_keys("pass2")
        first_name_field = self.driver.find_element_by_name("first_name")
        first_name_field.clear()
        first_name_field.send_keys("User2FirstName")
        last_name_field = self.driver.find_element_by_name("last_name")
        last_name_field.clear()
        last_name_field.send_keys("User2LastName")
        email_field = self.driver.find_element_by_name("email")
        email_field.clear()
        email_field.send_keys("user2@email.com")
        skype_account_field = self.driver.find_element_by_name("skype_account")
        skype_account_field.clear()
        skype_account_field.send_keys("user2.skype")
        has_hangout_field = self.driver.find_element_by_name("has_hangout")
        has_hangout_field.click()
        submit_button = self.driver.find_element_by_name("apply")
        submit_button.click()
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.NAME , "success")))
        self.driver.get(config.HostURL + "/user/list")
        usernames = self.driver.find_elements_by_class_name("username_list_field")
        index = len(usernames)-1
        assert usernames[index].text == "user2"
        firstnames = self.driver.find_elements_by_class_name("first_name_list_field")
        assert self.get_inner_text(firstnames[index]) == "User2FirstName"
        lastnames = self.driver.find_elements_by_class_name("last_name_list_field")
        assert self.get_inner_text(lastnames[index]) == "User2LastName"
        emails = self.driver.find_elements_by_class_name("email_list_field")
        assert emails[index].text == "user2@email.com"
        has_hangouts = self.driver.find_elements_by_class_name("has_hangout_list_field")
        assert self.get_inner_text(has_hangouts[index]) == "yes"
        skype_accounts = self.driver.find_elements_by_class_name("skype_account_list_field")
        assert skype_accounts[index].text == "user2.skype"
        self.driver.get(config.HostURL + "/user/logout")
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("user2")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("pass2")
        submit_button = self.driver.find_element_by_name("submit")
        submit_button.click()
        assert config.HostURL + "/video/list/" in self.driver.current_url
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.CLASS_NAME , "table")))
        self.driver.get(config.HostURL + "/user/list")
        delete_link = self.driver.find_element_by_name("delete_link" + user_id)
        delete_link.click()
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.ID , "error-self_destruct")))
        self.login()
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.CLASS_NAME , "table")))
        self.driver.get(config.HostURL + "/user/list")
        delete_link = self.driver.find_element_by_name("delete_link" + user_id)
        delete_link.click()
        WebDriverWait(self.driver,10).until(expected_conditions.presence_of_element_located((By.NAME , "success")))
        try:
            self.driver.find_element_by_name("delete_link" + user_id)
            assert False
        except:
            pass




if __name__ == "__main__":
    unittest.main()
