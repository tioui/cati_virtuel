import config
import unittest
from selenium.webdriver.common.keys import Keys


class Base_Test(unittest.TestCase):

    def setUp(self):
        self.driver = config.driver()

    def login(self):
        self.driver.get(config.HostURL + "/user/logout")
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("test")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("test")
        password_field.send_keys(Keys.RETURN)

    def get_inner_text(self, a_element):
        l_html = self.driver.execute_script("return document.getElementById('" + a_element.get_attribute("id") + "').innerHTML")
        return l_html.strip()

    def tearDown(self):
        self.driver.close()
