import config
import unittest
import base_test
from selenium.webdriver.common.keys import Keys


class Login(base_test.Base_Test):

    def test_login_button_main(self):
        self.driver.get(config.HostURL + "/user/logout")
        assert config.HostURL + "/user/login" in self.driver.current_url
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("test")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("test")
        submit_button = self.driver.find_element_by_name("submit")
        submit_button.click()
        assert config.HostURL + "/video/list/" in self.driver.current_url

    def test_login_return_main(self):
        self.driver.get(config.HostURL + "/user/logout")
        assert config.HostURL + "/user/login" in self.driver.current_url
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("test")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("test")
        password_field.send_keys(Keys.RETURN)
        assert config.HostURL + "/video/list/" in self.driver.current_url

    def test_login_wrong_username(self):
        self.driver.get(config.HostURL + "/user/logout")
        assert config.HostURL + "/user/login" in self.driver.current_url
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("whatever")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("test")
        password_field.send_keys(Keys.RETURN)
        assert config.HostURL + "/user/login/" in self.driver.current_url
        try:
            self.driver.find_element_by_name("error")
        except:
            assert False

    def test_login_wrong_password(self):
        self.driver.get(config.HostURL + "/user/logout")
        assert config.HostURL + "/user/login" in self.driver.current_url
        username_field = self.driver.find_element_by_name("username")
        username_field.send_keys("test")
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys("not good password")
        password_field.send_keys(Keys.RETURN)
        assert config.HostURL + "/user/login/" in self.driver.current_url
        try:
            self.driver.find_element_by_name("error")
        except:
            assert False


if __name__ == "__main__":
    unittest.main()
